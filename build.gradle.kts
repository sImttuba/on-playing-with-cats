plugins {
    kotlin("jvm") version "1.3.21"
    kotlin("kapt") version "1.3.21"
    `kotlin-dsl` version "1.2.2" // throws warning
}

group = "org.simttuba"
version = "1.0-SNAPSHOT"

val arrow_version: String by project
val kotlin_version: String by project
val klaxon_version: String by project
val ktor_version: String by project

repositories {
    mavenCentral()
    jcenter()
    maven {
        url = uri("https://dl.bintray.com/spekframework/spek-dev")
        content{ includeGroup("io.arrow-kt")}
    }
    maven {
        url = uri("https://oss.jfrog.org/artifactory/oss-snapshot-local/")
        content{ includeGroup("io.arrow-kt")}
    }
    maven {
        url = uri("https://dl.bintray.com/kotlin/kotlinx")
    }
}

dependencies {
    compile("org.jetbrains.kotlin:kotlin-stdlib-jdk8:$kotlin_version")
    compile("io.arrow-kt:arrow-core:$arrow_version")
    compile("io.arrow-kt:arrow-docs:$arrow_version")
    compile("io.arrow-kt:arrow-core-data:$arrow_version")
    compile("io.arrow-kt:arrow-syntax:$arrow_version")
    compile("io.arrow-kt:arrow-typeclasses:$arrow_version")
    compile("io.arrow-kt:arrow-extras:$arrow_version")
    compile("io.arrow-kt:arrow-core-extensions:$arrow_version")
    compile("io.arrow-kt:arrow-extras-extensions:$arrow_version")
    kapt("io.arrow-kt:arrow-meta:$arrow_version")
    //ktor client
    compile("io.ktor:ktor-client-okhttp:$ktor_version")
    compile("com.beust:klaxon:$klaxon_version")
    
    compile("io.arrow-kt:arrow-effects-io-extensions:$arrow_version")
    compile("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.1.1")
    compile("org.jetbrains.kotlinx:kotlinx-collections-immutable:0.1")
    compile("io.arrow-kt:arrow-free:$arrow_version") //optional
    compile("io.arrow-kt:arrow-free-extensions:$arrow_version") //optional
    compile("io.arrow-kt:arrow-mtl:$arrow_version") //optional
    compile("io.arrow-kt:arrow-effects:$arrow_version") //optional
    compile("io.arrow-kt:arrow-effects-extensions:$arrow_version") //optional
    compile("io.arrow-kt:arrow-effects-io-extensions:$arrow_version") //optional
    compile("io.arrow-kt:arrow-effects-rx2-extensions:$arrow_version") //optional
    compile("io.arrow-kt:arrow-effects-reactor:$arrow_version") //optional
    compile("io.arrow-kt:arrow-effects-reactor-extensions:$arrow_version") //optional
    compile("io.arrow-kt:arrow-effects-kotlinx-coroutines:$arrow_version") //optional
    compile("io.arrow-kt:arrow-effects-kotlinx-coroutines-data:$arrow_version") //optional
    compile("io.arrow-kt:arrow-effects-kotlinx-coroutines-extensions:$arrow_version") //optional
    compile("io.arrow-kt:arrow-optics:$arrow_version") //optional
    compile("io.arrow-kt:arrow-generic:$arrow_version") //optional
    compile("io.arrow-kt:arrow-recursion:$arrow_version") //optional
    compile("io.arrow-kt:arrow-recursion-extensions:$arrow_version") //optional
    compile("io.arrow-kt:arrow-integration-retrofit-adapter:$arrow_version") //optional
}
