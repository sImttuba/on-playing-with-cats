package powerOfOptics

import arrow.optics.*
import arrow.optics.Address
import arrow.optics.Employee
import arrow.optics.Company
import arrow.optics.Street

@optics data class Street(val number:Int, val name:String){
    companion object
}
@optics data class Address(val city:String, val street: Street){
    companion object
}
@optics data class Company(val name:String, val address: Address){
    companion object
}
@optics data class Employee(val name: String, val company: Company?){
    companion object
}

// either with composing

val employeeName=
    {employee:Employee, f: (String) -> String ->
        Employee.name.modify(employee, f)}

val employeeCompany =
    {emp:Employee, f: (Company) -> Company ->
        Employee.company.modify(emp, f)
    }

val companyAddress=
    {c:Company, f: (Address) -> Address ->
      Company.address.modify(c, f)}

val addressStreet=
    {ad:Address, f: (Street) -> Street ->
        Address.street.modify(ad, f)}

val employeeAddress =
    {e:Employee, f: (Address) -> Address ->
        employeeCompany(e) { c:Company -> companyAddress(c, f)}
    }

val employeeStreet =
    {e:Employee, f: (Street) -> Street ->
        employeeAddress(e){ a:Address -> addressStreet(a, f) }
    }

// or directly using the arrow dsl
val simpleEmployeeStreetName =
    {e:Employee , f: (String) -> String ->
        Employee.company.address.street.name.modify(e,f)}

