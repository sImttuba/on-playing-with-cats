package powerOfOptics

import arrow.optics.Optional
import arrow.core.*
import arrow.core.extensions.monoid
import arrow.data.*
import arrow.optics.*
import arrow.optics.Address
import arrow.optics.Company
import arrow.optics.Employee
import arrow.optics.Street
import arrow.effects.extensions.io.fx.fx
import kotlinx.coroutines.CoroutineStart
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking


//Optional and Lenses for immutable Datatypes
 val john:Employee = Employee(
    name = "John Doe",
    company = Company("Kategory",
        Address("Functional city", Street(44, "lamda Street"))
    ))

val lifted = ListK.head<Int>().lift { it * 5 }

@optics data class Participant(val name:String, val email:String?){
    companion object
}

val participantEmail : Optional<Participant, String> =  Optional(
    getOrModify = {p -> p.email?.right() ?: p.left() },
    set = {p, email -> p.copy(email = email)}
)

val triedEmail: Optional<Try<Participant>, String> =
        Try.success<Participant>() compose participantEmail

val b : (String) -> Option<String> = { new -> triedEmail.
    getOption(Try.Success(Participant("test", new)))}

fun main() {
    String.monoid().run{empty()}
}

infix fun <A, B, C> ((A) -> B).compose(g:(B) -> C) =
    { a:A -> g.invoke(this.invoke(a))}

fun f() = {i:Int -> when(i) {1 -> true; else -> false}}

fun g() = {b:Boolean -> if(b) "Hurei!!" else "I am Mad!"}

fun c() = f() compose g()

//val s = triedEmail.getOrModfy(Try.Failure(throw RuntimeException("ju")))

