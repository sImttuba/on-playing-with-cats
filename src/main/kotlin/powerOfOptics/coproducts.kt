package powerOfOptics

import arrow.generic.*
import arrow.generic.coproduct3.*

typealias SearchResult = Coproduct3<Car, Dealership, Salesperson>

fun toDisplayValues(items: List<SearchResult>): List<String> {
    return items.map {
        it.fold(
            { "Car: Speed: ${it.speed.kmh}" },
            { "Dealership: ${it.location}" },
            { "Salesperson: ${it.name}"}
        )
    }
}

typealias ApiResult = Coproduct3<CommonServerError, RegistrationError, Registration>

