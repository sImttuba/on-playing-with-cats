package rxCoro

/*Property Delegation
*  @Delegates lateinit on RunTime OnlyFor var
*  @Delegates by lazy {λ}
* */

lateinit var notNullStr:String
lateinit var notInit:String

val myLazyVal:String by lazy {
    println("Just Initialised")
    "My Lazy Val"
}

fun main(args: Array<String>) {
    notNullStr = "Initial value"
    println(notNullStr)
    //println(notInit)
    print(myLazyVal)

}