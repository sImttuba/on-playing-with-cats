package rxCoro

import arrow.Kind
import arrow.data.ListK
import arrow.effects.typeclasses.Async
import arrow.optics.Company
import javaslang.control.Either

// async made polymorphic
// WIP

interface EmployeeApiDataSource<F>{
    fun publicEmployeesforCompany(c:Company):Kind<F, ListK<Company>>
}

// instance
/*
class DefaultEmployeeApiDataSource<F>(private val async: Async<F>)
    : EmployeeApiDataSource<F>, Async<F> by async{
    override fun publicEmployeesforCompany(c: Company): Kind<F, ListK<Company>> =
            async{proc: (Either<Throwable, ListK<Company>>) -> Unit ->
                "http..".http

            }


}*/
