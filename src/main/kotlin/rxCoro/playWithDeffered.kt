package rxCoro
import arrow.effects.IO
import arrow.effects.coroutines.DeferredK
import arrow.effects.coroutines.unsafeRunAsync
import arrow.effects.extensions.io.monad.binding
import arrow.effects.fix
import kotlinx.coroutines.*
fun main() {
    GlobalScope.launch {
        delay(1000L)
        println("World!")
    }
    binding {
        val (a) = IO.invoke { 1 }
        a + 1
    }.fix().unsafeRunSync()
    val deferred =
    DeferredK {throw RuntimeException("Boom") }.unsafeRunAsync {
      it.fold({DeferredK{ print("Error found")}},
          {res -> DeferredK { println()}})
     }

    //runBlocking { deferred }

    /*IO.async()
        .async { callback:(Either<Throwable, Int>) -> Unit ->
            callback(RuntimeException().left())
        }.fix().attempt().unsafeRunSync().let { print(it.toString()) }*/
}
