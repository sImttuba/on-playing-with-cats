package rxCoro

import arrow.optics.*
sealed class Gender{
    object Male:Gender()
    object Female:Gender()
}

typealias UsersId = Int

@optics data class User(
    val id:UsersId, val fName:String,val lName:String,
    val gender: Gender
){companion object }

@optics data class Fact(val id:Int, val value:String, val user: User? = null){
    companion object
}

interface UserService{
    fun getFact(id: UsersId):Fact
}
