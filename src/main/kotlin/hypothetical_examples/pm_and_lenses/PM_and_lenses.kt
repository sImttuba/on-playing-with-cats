package hypothetical_examples.pm_and_lenses

import arrow.core.Try
import arrow.core.getOrDefault
import io.ktor.client.HttpClient
import io.ktor.client.call.call
import io.ktor.client.engine.okhttp.OkHttp
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode

val arrow = "https://arrow-kt.io/docs/"
val surrealism = "https://www.artsy.net/article/artsy-editorial-what-is-surrealism"
val amazon = "https://www.amazon.com/"


val client = HttpClient(OkHttp)

suspend fun urlResponse(url: String) =
    Try {
        client.call(url) {
            method = HttpMethod.Get
        }.let {
            it.response.status
        }
    }.getOrDefault { HttpStatusCode(-1, "Something went wrong") }
