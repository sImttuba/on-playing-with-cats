package hypothetical_examples.pm_and_lenses

import arrow.core.Try
import arrow.core.getOrDefault
import arrow.optics.optics
import arrow.optics.*
import com.beust.klaxon.Json
import com.beust.klaxon.Klaxon
import io.ktor.client.HttpClient
import io.ktor.client.engine.okhttp.OkHttp
import io.ktor.client.request.get
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

// Well the true power of lenses and optics can bd exploited through pattern matching (PM) a json file

@optics// (2) then the lenses are auto-generated
data class Jokes(// (1) here the values are filled with PM
    @Json
    val id: Int,
    @Json
    val joke: String,
    @Json
    val categories: List<String>
){
    companion object// (3) the companion object is important to traverse through nested Types like ChuckJason
}

@optics
data class ChuckJason(
    @Json
    val type: String,
    @Json
    val value: Jokes
){
    companion object // here too
}

suspend fun HttpClient.sabotage(url: String): String {
    return get<String>(url)
    //throw Throwable("Gotcha") // making it resilient against any unwanted exceptions
}

suspend fun htmlContent() =
    Try {
        // you can change the client if you like
        HttpClient(OkHttp).use {
            it.sabotage("http://api.icndb.com/jokes/random/")
        }
            .run { (Klaxon().parse<ChuckJason>(this) as ChuckJason) }
            .run { value.joke }
    }
        .getOrDefault { "\nChuck Norris is currently sleeping - theoretically this is against physics. " }

fun main() = runBlocking {
    coroutineScope{
        launch{
            print(htmlContent())
        }
    }
    print(2)
}