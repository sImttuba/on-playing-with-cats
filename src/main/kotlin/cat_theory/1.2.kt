package cat_theory

import arrow.core.Eval
import arrow.core.compose
import arrow.core.identity
import arrow.data.WriterT
import arrow.syntax.function.memoize
import kotlin.random.Random
import arrow.reflect.*
import arrow.data.*
import arrow.core.*
import arrow.typeclasses.ApplicativeError
import arrow.typeclasses.Comonad

val f: (Int) -> Int = { it }
val identity: (Int) -> Int = ::identity

fun absurd(): (Nothing) -> Int =
    { _ -> 32 }

val a = Eval.later {
    f.also { print("foo") }
}

fun fak(i:Int):Int =
    (if(i == 1) 1 else i * fakMemoized(i -1))
        .also { println("computed for $i") }


val fakMemoized = {i:Int -> fak(i)}.memoize()

val randomMemoized = {until:Int -> Random.nextInt(until)}.memoize()

val randomSeedMemoized =
    {seed:Int -> Random(seed).nextInt()}.memoize()
val d = """
    kotlin:ank -> executes the snippet
    kotlin:ank:silent -> does not display the output of the snippet
    kotlin:ank:replace -> replaces the current snippet with its output
    kotlin:ank:fail -> this snippet will pass the gradle task and the stacktrace is displayable
    kotlin:ank:playground ->(
        with //sampleStart and //sampleEnd one specifies the displayable field.
        The rest can be expanded by the viewer:
    ).`example`{
    ```kotlin:ank:playground
        //sampleStart
        fun helloWorld(): String = "Hello World"
        //sampleEnd
        fun main() {
            println(helloWorld())
        }
    ```
    }
    kotlin:ank:outFile ??
    kotlin:ank:playground:extension ??
"""

fun main() {
    //println(fakMemoized(9))
    //println(fakMemoized(29))
}