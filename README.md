# Purpose of this Project
I am very curious, especially when it comes to FP Patterns. So, when ever I find the time in my spare time I will make a deep dive into one FP Pattern and try to make an small digestible example and push it here. 


Don't worry I won't bother you with Laws or anything I don't find usefull for applying these pattern. 

## My Personal Cheat Cheet 
Idea | basic Use case | Implements at least …
--- | --- | ---
`Functor f` | *WIP* | fmap :: (a -> b) -> f a -> f b
`Monad a` | sequential Computations | pure :: Monad a -> a `Λ` map :: (a -> b) -> (Monad a -> Monad b) `Λ` fmap
`Applicative f` | multiple idependent computations within silo's or DI| liftA2 :: (a -> b -> c) -> f a -> f b -> f c `Λ` pure `Λ` map `Λ` fmap 


## Some 'Aha' moments 😶
The **difference between fmap and map** is that map implies that the monadic contex stays coherent, where as fmap drills down to our Type we care about.
They are very similiar, only that fmap is an abstraction of map. 
## Added Examples
- [The Power of PM and Lenses combined](https://gitlab.com/sImttuba/on-playing-with-cats/blob/master/src/main/kotlin/hypothetical_examples/pm_and_lenses/model.kt)

## Upcoming or further examples
1. The Power of PM and Lenses(=immutable nested data access) // WIP
*  [Links to Arrow](https://arrow-kt.io/docs/optics/lens/)
*  [Still reading this one](https://leanpub.com/lenses)


2. Applicative(=independent computations) and FormValidation // Still in Design
*  [WIP](https://www.youtube.com/watch?v=ahCgYl0cYJ0&list=PLTx-VKTe8yLwNy3oXbbwsQRwoS-G8mhEh&index=3)

3. Brackets (=resource acquisition) and API's // Backlog
*  [WIP](https://arrow-kt.io/docs/arrow/effects/typeclasses/bracket/#bracket) with ktor🧡  